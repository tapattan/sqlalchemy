from config.config import __engine__,__host__,__port__,__username__,__password__,__databasename__
from sqlalchemy import  create_engine

def connDB():
   e = __engine__
   h = __host__
   p = __port__
   u = __username__ 
   pw = __password__
   db = __databasename__
   
   connectionString = e+'://'+u+':'+pw+'@'+h+':'+p+'/'+db

   engine = create_engine(connectionString)
   return engine