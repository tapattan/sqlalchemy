-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 03, 2022 at 05:06 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `dbTrain`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbmember`
--

CREATE TABLE `tbmember` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `mobile` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbmember`
--

INSERT INTO `tbmember` (`id`, `name`, `email`, `age`, `mobile`) VALUES
(1, 'lisa', 'lisa2022@gmail.com', 18, '0888888888'),
(2, 'george', 'george@gmail.com', 20, '0999999999');

-- --------------------------------------------------------

--
-- Table structure for table `tbproduct`
--

CREATE TABLE `tbproduct` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbproduct`
--

INSERT INTO `tbproduct` (`id`, `name`, `code`, `price`) VALUES
(1, 'ปากกาน้ำเงิน', 'P01', '10'),
(2, 'สมุด', 'B01', '20'),
(3, 'ยางลบ', 'R01', '5'),
(4, 'ปากกาแดง', 'P02', '12');

-- --------------------------------------------------------

--
-- Table structure for table `tbtransaction`
--

CREATE TABLE `tbtransaction` (
  `id` int(11) NOT NULL,
  `memberid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbtransaction`
--

INSERT INTO `tbtransaction` (`id`, `memberid`, `productid`, `qty`, `date`) VALUES
(1, 1, 1, 2, '2022-02-03 23:11:06'),
(2, 1, 2, 1, '2022-02-03 23:11:06'),
(3, 1, 3, 1, '2022-02-03 23:11:33'),
(4, 2, 4, 2, '2022-02-03 23:11:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbmember`
--
ALTER TABLE `tbmember`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbproduct`
--
ALTER TABLE `tbproduct`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtransaction`
--
ALTER TABLE `tbtransaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1` (`memberid`),
  ADD KEY `FK2` (`productid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbmember`
--
ALTER TABLE `tbmember`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbproduct`
--
ALTER TABLE `tbproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbtransaction`
--
ALTER TABLE `tbtransaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbtransaction`
--
ALTER TABLE `tbtransaction`
  ADD CONSTRAINT `FK1` FOREIGN KEY (`memberid`) REFERENCES `tbmember` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2` FOREIGN KEY (`productid`) REFERENCES `tbproduct` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
