#CrudSevices.py
from serviceCon import connectionServices
from modelMember import Tbmember

class TbmemberCRUD(connectionServices):
    def add(self,member:Tbmember):
       try:
         self.db.begin()
         self.db.add(member)
         self.db.commit()

         self.db.refresh(member)
         print(member.id)
         return True
       except exc.SQLAlchemyError as e:
         return e,False