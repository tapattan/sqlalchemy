#serviceCon.py
from sqlalchemy import  create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session
from sqlalchemy import exc

#mysql local
engine = create_engine("mysql+mysqlconnector://george:1234@localhost:3306/dbTrain")
session = sessionmaker(autocommit=True, autoflush=True, bind=engine)

class connectionServices():
      def __init__(self, con):
         self.db = con()