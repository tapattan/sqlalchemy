#modelMember.py
# coding: utf-8
from sqlalchemy import Column, DECIMAL, DateTime, ForeignKey, String, text
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Tbmember(Base):
    __tablename__ = 'tbmember'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)
    age = Column(INTEGER(11), nullable=False)
    mobile = Column(String(255), nullable=False)