# coding: utf-8
from sqlalchemy import Column, DECIMAL, DateTime, ForeignKey, String, text
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Tbmember(Base):
    __tablename__ = 'tbmember'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)
    age = Column(INTEGER(11), nullable=False)
    mobile = Column(String(255), nullable=False)


class Tbmonthlyreport(Base):
    __tablename__ = 'tbmonthlyreport'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(255), nullable=False)
    price = Column(INTEGER(11), nullable=False)
    date = Column(String(255), nullable=False)


class Tbproduct(Base):
    __tablename__ = 'tbproduct'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(255), nullable=False)
    code = Column(String(255), nullable=False)
    price = Column(DECIMAL(10, 0), nullable=False)


class Tbtransaction(Base):
    __tablename__ = 'tbtransaction'

    id = Column(INTEGER(11), primary_key=True)
    memberid = Column(ForeignKey('tbmember.id'), nullable=False, index=True)
    productid = Column(ForeignKey('tbproduct.id'), nullable=False, index=True)
    qty = Column(INTEGER(11), nullable=False)
    date = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))

    tbmember = relationship('Tbmember')
    tbproduct = relationship('Tbproduct')
